﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Shell;
using System.Windows.Threading;

namespace Murderer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private List<Process> _processes;
        public List<Process> Processes
        {
            get { return _processes; }
            set { SetField(ref _processes, value); }
        }

        // boiler-plate
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public MainWindow()
        {
            var timer = new DispatcherTimer(
                TimeSpan.FromSeconds(10),
                DispatcherPriority.Normal,
                (sender, eArgs) =>
                {
                    RefreshProcessList();
                    CreateJumpList();
                },
                Dispatcher);

            InitializeComponent();

            RefreshProcessList();

            CreateJumpList();
        }

        private void RefreshProcessList()
        {
            var procs = new List<Process>(Process.GetProcesses());
            procs.Sort((proc1, proc2) => proc2.WorkingSet64.CompareTo(proc1.WorkingSet64));

            Processes = procs;
        }

        private void CreateJumpList()
        {
            var jumpList = new JumpList();
            JumpList.SetJumpList(Application.Current, jumpList);

            var thisAppPath = Assembly.GetEntryAssembly().Location;

            foreach (var process in Processes)
            {
                jumpList.JumpItems.Add(new JumpTask
                {
                    Title = string.Format("{0}-{1}-{2}", process.ProcessName, process.Id, FormatMemory(process.WorkingSet64)),
                    Description = string.Format("kill {0}", process.ProcessName),
                    ApplicationPath = thisAppPath,
                    Arguments = string.Format("/kill {0}", process.Id),
                });
            }

            jumpList.Apply();
        }

        public static string FormatMemory(long memory)
        {
            var suffixes = new[] { "B", "KB", "MB", "GB", "TB" };
            var sufIndex = 0;

            while (sufIndex < suffixes.Length & memory / 1024 > 0)
            {
                memory /= 1024;
                ++sufIndex;
            }

            return string.Format("{0}{1}", memory, suffixes[sufIndex]);
        }

        public bool ProcessCommandLineArgs(IList<string> args)
        {
            if (args == null || args.Count == 0)
                return true;

            var thisAppPath = args[0];
            var action = args[1];
            var id = args[2];

            MessageBox.Show(string.Format("{0} -> {1}", action, id));

            return true;
        }

        private void Kill_Process_Click(object sender, RoutedEventArgs e)
        {
            if (ProcessList.SelectedItems.Count == 0)
                return;

            var killProcs = new List<Process>(ProcessList.SelectedItems.Count);
            var listCopy = new Process[ProcessList.SelectedItems.Count];
            ProcessList.SelectedItems.CopyTo(listCopy, 0);

            foreach (Process proc in listCopy)
            {
                if (MessageBox.Show(
                    string.Format("Really kill the following Process?\r\nID:{0}\r\nName:{1}\r\nTitle:{2}",
                        proc.Id,
                        proc.ProcessName,
                        proc.MainWindowTitle),
                        "Are you sure?",
                    MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    //proc.Kill();
                    MessageBox.Show(string.Format("Killing {0}", proc.Id));
                    killProcs.Add(proc);
                }
            }

            killProcs.ForEach(p => p.Kill());
        }
    }
}
