﻿//http://elegantcode.com/2011/03/02/wpf-advanced-jump-lists-using-a-single-instance-application/

using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Shell;

namespace Murderer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, ISingleInstanceApp
    {
        [STAThread]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance("AdvancedJumpList"))
            {
                var application = new App();
                application.Init();
                application.Run();

                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }

        public void Init()
        {
            InitializeComponent();
        }

        #region ISingleInstanceApp Members

        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            return ((MainWindow) MainWindow).ProcessCommandLineArgs(args);
        }

        #endregion
    }
}
